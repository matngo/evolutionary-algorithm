import numpy as np

from kws_ga.common import Registrable


class Individual(Registrable):
    _id = 0

    def __init__(self):
        self.id = type(self)._id
        type(self)._id += 1
        self.chromosome = self._initialize()
        self._compute_fitness()

    def __lt__(self, other):
        return self.fitness < other.fitness

    def __le__(self, other):
        return self.fitness <= other.fitness

    def __gt__(self, other):
        return other < self

    def __ge__(self, other):
        return other <= self

    def _compute_fitness(self):
        raise NotImplementedError

    def _initialize(self):
        raise NotImplementedError

    def __repr__(self):
        return f"<Individual {self.id} - fitness: {self.fitness}>"

