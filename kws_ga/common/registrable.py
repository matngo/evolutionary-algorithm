import inspect
import logging
from collections import defaultdict
from typing import Any, Dict, List, Type, TypeVar, cast

import numpy as np

T = TypeVar("T")


def takes_arg(obj, arg: str) -> bool:
    if inspect.isclass(obj):
        signature = inspect.signature(obj.__init__)
    elif inspect.isfunction(obj) or inspect.ismethod(obj):
        signature = inspect.signature(obj)

    return arg in signature.parameters


def to_annotation_type(obj, annotation):
    if annotation is np.ndarray:
        return np.array(obj)
    else:
        return annotation(obj)


def create_kwargs(cls, params: Dict[str, Any], **kwargs) -> Dict[str, Any]:
    signature = inspect.signature(cls.__init__)
    new_params: Dict = {}

    for name, param in signature.parameters.items():
        if name == "self":
            continue

        annotation = param.annotation

        if name in params:
            arg = params.pop(name)
            if hasattr(annotation, "from_dict"):
                new_params[name] = annotation.from_dict(params=arg, **kwargs)

            else:
                new_params[name] = annotation(arg)

        elif name in kwargs:
            new_params[name] = to_annotation_type(kwargs[name], annotation)

        elif param.default != inspect._empty:
            new_params[name] = param.default

        else:
            raise AttributeError(f"Missing argument {name} for class {cls}")

    return new_params


class FromDict(object):
    @classmethod
    def from_dict(cls: Type[T], params: Dict[str, Any], **kwargs) -> T:
        registered_subclass = Registrable._registry.get(cls)

        # checks if it is a base class
        if registered_subclass is not None:
            subclass_name = params.pop("type")
            subclass = registered_subclass[subclass_name]
            return subclass.from_dict(params, **kwargs)

        else:
            new_params = create_kwargs(cls, params, **kwargs)
            return cls(**new_params, **kwargs)  # type: ignore


class Registrable(FromDict):
    """
    Any class that inherits from ``Registrable`` gains access to a named registry for its
    subclasses. To register them, just decorate them with the classmethod
    ``@BaseClass.register(name)``.
    After which you can call ``BaseClass.list_available()`` to get the keys for the
    registered subclasses, and ``BaseClass.by_name(name)`` to get the corresponding subclass.
    Note that the registry stores the subclasses themselves; not class instances.
    In most cases you would then call ``from_params(params)`` on the returned subclass.
    You can specify a default by setting ``BaseClass.default_implementation``.
    If it is set, it will be the first element of ``list_available()``.
    Note that if you use this class to implement a new ``Registrable`` abstract class,
    you must ensure that all subclasses of the abstract class are loaded when the module is
    loaded, because the subclasses register themselves in their respective files. You can
    achieve this by having the abstract class and all subclasses in the __init__.py of the
    module in which they reside (as this causes any import of either the abstract class or
    a subclass to load all other subclasses and the abstract class).
    """

    _registry: Dict[Type, Dict[str, Type]] = defaultdict(dict)

    @classmethod
    def register(cls: Type[T], name: str):
        """
        Register a class under a particular name.
        Parameters
        ----------
        name:
            The name to register the class under.
        """
        registry = Registrable._registry[cls]

        def add_subclass_to_registry(subclass: Type[T]):
            # Add to registry, raise an error if key has already been used.
            if name in registry:
                raise ValueError(f"{name} already exists")

            registry[name] = subclass
            return subclass

        return add_subclass_to_registry

    @classmethod
    def by_name(cls: Type[T], name: str) -> Type[T]:
        if name not in Registrable._registry[cls]:
            print("%s is not a registered name for %s" % (name, cls.__name__))
        return Registrable._registry[cls].get(name)

    @classmethod
    def list_available(cls) -> List[str]:
        """List default first if it exists"""
        return list(Registrable._registry[cls].keys())

    def __repr__(self) -> str:
        params = "\n".join(
            (f"{name} = {self.__getattribute__(name)}" for name in self.__slots__)
        )
        return f"{self.__class__.__name__}({params})"
