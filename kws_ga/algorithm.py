from kws_ga.common import Registrable

class GeneticAlgorithm(Registrable):
    def __init__(self, individual_type, selection, crossover, mutation, population_size, nb_generation):
        self.population = [individual_type() for _ in range(population_size)]
        self.selection = selection
        self.crossover = crossover
        self.mutation = mutation
        self.nb_generation = nb_generation

    def run(self):
        for i in range(self.nb_generation):
            print(i)
            mating_pool = self.selection(self.population)
            children = self.crossover(mating_pool)
            self.mutation(children)
            for child in children:
                child._compute_fitness()

            self.population = mating_pool + children

        return self.population