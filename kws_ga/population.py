from kws_ga.common import Registrable


class Population(Registrable):
    def __init__(self, individual_type, key, nb_individuals):
        self.individuals = [individual_type() for _ in range(nb_individuals)]
        self.key = key

    def sort(self):
        self.individuals = sorted(self.individuals, key=self.key)

    def compute_fitness(self):
        for individual in individuals:
            individual.compute_fitness()

    def __getitem__(self, val):
        self.individuals = self.individuals[val]
        return self