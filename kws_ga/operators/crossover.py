from copy import deepcopy

import numpy as np

from kws_ga.common import Registrable


class Crossover(Registrable):
    def __init__(self, children_pool_size, **kwargs):
        self.children_pool_size = children_pool_size

    def _crossover(self, p1, p2):
        raise NotImplementedError

    def _get_parents(self, population):
        return np.random.choice(population, 2)

    def __call__(self, population):
        children = list()
        while len(children) < self.children_pool_size:
            p1, p2 = self._get_parents(population)
            children += (self._crossover(p1, p2))

        return children


@Crossover.register("sbx-crossover")
def SBXCrossover(Crossover):
    def __init__(self, children_pool_size, beta, **kwargs):
        super().__init__(children_pool_size)
        self.beta = beta

    def _crossover(self, p1, p2):
        c1, c2 = deepcopy(p1), deepcopy(p2)
        x_bar = (c1.chromosome + c2.chromosome) / 2
        c1.chromosome = x_bar - 0.5 * self.beta * (c1.chromosome)
        c2.chromosome = x_bar - 0.5 * self.beta * (c2.chromosome)
        return c1, c2


@Crossover.register("one-point-crossover")
class OnePointCrossover(Crossover):
    def _crossover(self, p1, p2):
        c1, c2 = deepcopy(p1), deepcopy(p2)
        point = np.random.randint(0, len(p1.chromosome))
        c1.chromosome = np.concatenate((p1.chromosome[:point], p2.chromosome[point:]))
        c2.chromosome = np.concatenate((p2.chromosome[:point], p1.chromosome[point:]))

        return c1, c2
