import random

import numpy as np

from kws_ga.common import Registrable


class Mutation(Registrable):
    def __init__(self, mutation_rate, **kwargs):
        self.mutation_rate = mutation_rate

    def _mutate(self, indivdual):
        raise NotImplementedError

    def __call__(self, population):
        for individual in population:
            mutation = self._mutate(individual)
            is_mutated = np.array(
                [
                    1 if random.random() < self.mutation_rate else 0
                    for _ in range(len(mutation))
                ]
            )
            individual.chromosome[is_mutated] = mutation[is_mutated]


Mutation.register("binary-mutation")


class BinaryMutation(Mutation):
    def _mutate(self, individual):
        return np.logical_not(individual.chromosome).astype("int8")
