from kws_ga.common import Registrable


class Selection(Registrable):
    def __init__(self, mating_pool_size, **kwargs):
        self.mating_pool_size = mating_pool_size

    def _select(self, population):
        raise NotImplementedError

    def __call__(self, population):
        return self._select(population)


@Selection.register("elitist")
class ElitistSelection(Selection):
    def _select(self, population):
        population.sort(reverse=True)
        return population[: self.mating_pool_size]

