import numpy as np

from kws_ga.algorithm import GeneticAlgorithm
from kws_ga.individuals import Individual
from kws_ga.operators.mutation import BinaryMutation, NormalMutation
from kws_ga.operators.crossover import OnePointCrossover, SBXCrossover, DoNothing
from kws_ga.operators.selection import (
    RouletteSelection,
    ElitistSelection,
    RandomSelection,
)


@Individual.register("one-max")
class OneMax(Individual):
    def _compute_fitness(self):
        self.fitness = np.sum(self.chromosome)

    def _initialize(self):
        return np.random.randint(0, 2, 1000)


@Individual.register("ackley")
class Ackley(Individual):
    def _compute_fitness(self):
        self.chromosome = np.clip(self.chromosome, -5, 5)
        X = self.chromosome
        A = 20 * np.exp(-0.2 * np.sqrt(0.5 * np.sum(X ** 2)))
        B = np.exp(0.5 * np.sum(np.cos(2 * np.pi * X)))
        self.fitness = -(np.exp(1) + 20 - A - B)

    def _initialize(self):
        return np.random.randn(2) * 100 + 2


class Sphere(Individual):
    def _compute_fitness(self):
        self.fitness = -np.sum(self.chromosome ** 2)

    def _initialize(self):
        return np.random.randn(5) * 100


class Rosenbrock(Individual):
    def _compute_fitness(self):
        X = self.chromosome[:-1]
        X2 = self.chromosome[1:]
        self.fitness = -np.sum((X2 - X ** 2) ** 2 + (1 - X) ** 2)

    def _initialize(self):
        return np.random.randn(100) * 100


if __name__ == "__main__":
    algo = GeneticAlgorithm(
        individual_type=Ackley,
        selection=ElitistSelection(50),
        crossover=SBXCrossover(50, beta=0.01),
        mutation=NormalMutation(0.1, mu=0, sigma=0.5),
        population_size=50,
        nb_generation=2000,
    )

    new_pop = algo.run()
    new_pop.sort(reverse=True)
    print(new_pop[0].fitness, new_pop[0].chromosome)
