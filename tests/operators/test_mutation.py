import pytest

import numpy as np

from kws_ga.operators.mutation import BinaryMutation


@pytest.fixture
def one_individual():
    class Individual:
        def __init__(self, chromosome):
            self.chromosome = chromosome

    return Individual(np.ones(10))


@pytest.fixture
def binary_mutation():
    return BinaryMutation(1)


def test_binary(one_individual, binary_mutation):
    assert np.all(
        binary_mutation._mutate(one_individual)
        == np.zeros_like(one_individual.chromosome)
    )

